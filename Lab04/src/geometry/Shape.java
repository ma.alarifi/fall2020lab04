//Mishal Alarifi 1942365
package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();
}
