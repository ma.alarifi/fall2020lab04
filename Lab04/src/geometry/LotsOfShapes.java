//Mishal Alarifi 1942365
package geometry;

public class LotsOfShapes {
	public static void main(String[] args){
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(5, 10);
		shapes[1] = new Rectangle(3, 7);
		shapes[2] = new Circle(8);
		shapes[3] = new Circle(17);
		shapes[4] = new Square(9);
		
		for(int i = 0; i < shapes.length; i++){
			System.out.println("Perimeter: " + shapes[i].getPerimeter() + " Area: " + shapes[i].getArea());
		}
	}
	

}
