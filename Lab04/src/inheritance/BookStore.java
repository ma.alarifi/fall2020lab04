//Mishal Alarifi 1942365
package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] inventory = new Book[5];
		inventory[0] = new Book("Moby Dick", "Herman Melville");
		inventory[1] = new ElectronicBook("Lovecraft Country", "Matt Ruff", "13 bytes");
		inventory[2] = new Book("Dune", "Frank Herbert");
		inventory[3] = new ElectronicBook("Circe", "Madeleine Miller", "24 bytes");
		inventory[4] = new ElectronicBook("Black Leopard Red Wolf", "Marlon James", "37 bytes");
	
		for(int i = 0; i<inventory.length;i++){
			System.out.println(inventory[i]);
		}
	}
}
