//Mishal Alarifi 1942365
package inheritance;

public class Book {
	protected String title;
	private String authour;
	
	public Book(String title, String authour){
		this.title = title;
		this.authour = authour;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getAuthour(){
		return authour;
	}
	
	public String toString(){
		return "title: " + title + " authour: " + authour;
	}
}
