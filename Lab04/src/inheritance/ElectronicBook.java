//Mishal Alarifi 1942365
package inheritance;

public class ElectronicBook extends Book{
	private String numberBytes;
	
	public ElectronicBook(String title, String authour, String numberBytes){
		super(title, authour);
		this.numberBytes = numberBytes;
	}
	
	public String toString(){
		String fromBase = super.toString();
		return fromBase + " bytes: " + numberBytes;
	}
}
